use triger_fk_cursor
go

INSERT INTO employee (surname, name, midle_name, identity_number, passport, 
                      experience, birthday, post, pharmacy_id)
VALUES ('Petrov', 'Petro', 'Petrovich', '1011101010', '000111222', 10, '19850101', 'pharmacist', 1),
       ('Ivanov', 'Ivan', 'Ivanovich', '1010121056', '000111123', 15, '19800101', 'director', 1),
       ('Vaskov', 'Vasyl', 'Vasylyovich', '1020101010', '000111333', 5, '19900101', 'director', 2),
       ('Maslyakova', 'Mila', 'Volodymyrivna', '1030101010', '000111444', 2, '19930101', 'pharmacist', 2),
       ('Karpov', 'Kyrylo', 'Romanovych', '1011201010', '000111225', 8, '19870101', 'director', 3),
       ('Ivanchuk', 'Larysa', 'Bogdanivna', '1040101010', '000111454', 3, '19910101', 'pharmacist', 3)
go

select * from employee
go 
