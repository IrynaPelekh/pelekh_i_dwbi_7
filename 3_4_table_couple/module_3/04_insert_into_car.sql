use [auto_show]
go

INSERT INTO car (model, drive, height_mm, width_mm, wheelbase_mm, length_mm, engine_layout, cylinders_number,     engine_displacement, power_kW, top_speed_kmh, trunk_capacity,   fuel_tank, color, doors_number, price_usd, special_model, inserted_date)

VALUES  ('Porsche Panamera', 'Rear wheel drive', 1423, 1937, 2950, 5049, 'Front Engine', 6, 2995, 243, 264, 500, 75, 'black', 5, 115000, 0, getdate()),

('Porsche Panamera 4', 'All while drive', 1428, 1937, 3100, 5199, 'Front Engine', 6, 2995, 243, 278, 500, 75, 'white', 5, 117748, 1, getdate()),

('Porsche Panamera 4 Sport Turismo', 'All while drive', 1428, 1937, 2950, 5049, 'Front Engine', 6, 2995, 243, 264, 520, 75, 'black', 5, 121720, 1, getdate()),

('Porsche Panamera 4S',  'All while drive', 1423, 1937, 2950, 5049, 'Front Engine', 6, 2894, 324, 264, 500, 75, 'black', 5, 140000, 0, getdate()), 

('Porsche Panamera 4S Executive',  'All while drive', 1428, 1937, 3100, 5199, 'Front Engine', 6, 2894, 324, 264, 500, 75, 'while', 5, 155000, 1, getdate())





