use auto_show
go

CREATE OR ALTER VIEW car_view
      (id, model, drive, power_kW, top_speed_kmh, 
      trunk_capacity,   fuel_tank, color, price_usd)
AS
SELECT id, model, drive, power_kW, top_speed_kmh, 
       trunk_capacity,   fuel_tank, color, price_usd
FROM car