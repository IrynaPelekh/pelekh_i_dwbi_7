use auto_show
go

drop trigger if exists tr_customer 
go

---- trigger creation
create trigger tr_customer ON customer
AFTER UPDATE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)

if (@ins > 0 and @del > 0)  
	
	begin
		update customer 
		set updated_date = getdate()
		
		from customer 
			inner join inserted 
     on customer.contract_number = inserted.contract_number
	end

END
GO