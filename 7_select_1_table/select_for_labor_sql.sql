
--- task 1 ---
use labor_sql
go
SELECT maker, [type] 
FROM product
WHERE [type] like 'laptop'
ORDER BY maker ASC
go

--- task 2 ---
use labor_sql
go
SELECT model, ram, screen, price
FROM laptop
WHERE price > 1000
ORDER BY ram ASC, price DESC
go

--- task 3 ---
use labor_sql
go
SELECT * 
FROM printer
WHERE color = 'y'
ORDER BY price DESC
go

--- task 4 ---
use labor_sql
go
SELECT model, speed, hd, cd, price
FROM pc
WHERE (cd = '12x' OR cd = '24x') AND price < 600
ORDER BY speed DESC
go

--- task 5 ---
use labor_sql
go
SELECT name, class
FROM ships
WHERE name = class
ORDER BY name
go

--- task 6 ---
use labor_sql
go
SELECT *
FROM pc
WHERE (ram >= 500 AND price < 800)
ORDER BY price DESC
go

--- task 7 ---
use labor_sql
go
SELECT *
FROM printer
WHERE (type != 'Matrix' AND price < 300)
ORDER BY type DESC
go

--- task 8 ---
use labor_sql
go
SELECT model, speed
FROM pc
WHERE (price >= 400 AND price <=600)
ORDER BY hd
go

--- task 9 ---
use labor_sql
go
SELECT model, speed, hd, price
FROM laptop
WHERE screen >= 12
ORDER BY price DESC
go

--- task 10 ---
use labor_sql
go
SELECT model, type, price
FROM printer
WHERE price < 300
ORDER BY type DESC
go

--- task 11 ---
use labor_sql
go
SELECT model, ram, price
FROM laptop
WHERE ram = 64
ORDER BY screen
go

--- task 12 ---
use labor_sql
go
SELECT model, ram, price
FROM pc
WHERE ram > 64
ORDER BY hd
go

--- task 13 ---
use labor_sql
go
SELECT model, speed, price
FROM pc
WHERE speed BETWEEN 500 AND 750
ORDER BY hd DESC
go

--- task 14 ---
use labor_sql
go
SELECT *
FROM outcome_o
WHERE out > 2000
ORDER BY date DESC
go

--- task 15 ---
use labor_sql
go
SELECT *
FROM income_o
WHERE inc BETWEEN 5000 AND 10000
ORDER BY inc
go

--- task 16 ---
use labor_sql
go
SELECT *
FROM income
WHERE point = 1
ORDER BY inc
go

--- task 17 ---
use labor_sql
go
SELECT *
FROM outcome
WHERE point = 2
ORDER BY out
go

--- task 18 ---
use labor_sql
go
SELECT *
FROM classes
WHERE country = 'Japan'
ORDER BY type DESC
go

--- task 19 ---
use labor_sql
go
SELECT name, launched
FROM ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC
go

--- task 20 ---
use labor_sql
go
SELECT ship, battle, result
FROM outcomes
WHERE battle = 'Guadalcanal' AND result != 'sunk'
ORDER BY ship DESC
go

--- task 21 ---
use labor_sql
go
SELECT ship, battle, result
FROM outcomes
WHERE result = 'sunk'
ORDER BY ship DESC
go

--- task 22 ---
use labor_sql
go
SELECT class, displacement
FROM classes
WHERE displacement >= 40000
ORDER BY type
go

--- task 23 ---
use labor_sql
go
SELECT trip_no, town_from, town_to
FROM trip
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out
go

--- task 24 ---
use labor_sql
go
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane = 'TU-134'
ORDER BY time_out DESC
go

--- task 25 ---
use labor_sql
go
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane != 'IL-86'
ORDER BY plane
go

--- task 26 ---
use labor_sql
go
SELECT trip_no, town_from, town_to
FROM trip
WHERE town_from != 'Rostov' AND town_to != 'Rostov'
ORDER BY plane
go

--- task 27 ---
use labor_sql
go
SELECT model
FROM product
WHERE model LIKE '%1%1%' AND type = 'pc'
go

--- task 28 ---
use labor_sql
go
SELECT *
FROM outcome
WHERE month(date) = 3
go

--- task 29 ---
use labor_sql
go
SELECT *
FROM outcome_o
WHERE day(date) = 14
go

--- task 30 ---
use labor_sql
go
SELECT name
FROM ships
WHERE name LIKE 'W%n'
go

--- task 31 ---
use labor_sql
go
SELECT name
FROM ships
WHERE name LIKE '%e%e%'
go

--- task 32 ---
use labor_sql
go
SELECT name, launched
FROM ships
WHERE name NOT LIKE '%a'
go

--- task 33 ---
use labor_sql
go
SELECT name
FROM battles
WHERE name LIKE '% %[^c]'
go

--- task 34 ---
use labor_sql
go
SELECT *
FROM trip
WHERE datepart(hour,time_out) BETWEEN 12 AND 17
go

--- task 35 ---
use labor_sql
go
SELECT *
FROM trip
WHERE datepart(hour,time_in) BETWEEN 17 AND 23
go

--- task 36 ---
use labor_sql
go
SELECT *
FROM trip
WHERE (datepart(hour,time_in) BETWEEN 21 AND 23) OR (datepart(hour,time_in) BETWEEN 00 AND 10)
go

--- task 37 ---
use labor_sql
go
SELECT date
FROM pass_in_trip
WHERE place LIKE '1%'
go

--- task 38 ---
use labor_sql
go
SELECT date
FROM pass_in_trip
WHERE place LIKE '%c'
go

--- task 39 ---
use labor_sql
go
SELECT SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)) AS surname
FROM passenger
WHERE name LIKE '% C%'
go

--- task 40 ---
use labor_sql
go
SELECT SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)) AS surname
FROM passenger
WHERE name NOT LIKE '% J%'
go

--- task 41 ---
use labor_sql
go
SELECT ('average_price=' + str(AVG(price)))
FROM laptop
go

--- task 42 ---
use labor_sql
go
SELECT concat('code: ',code) AS code, concat('model:',model) AS model, concat('speed:',speed) AS speed,
        concat('ram:',ram) AS ram, concat('hd:',hd) AS hd, concat('cd:',cd) AS cd, concat('price:',price) AS price
FROM pc
go

--- task 43 ---
use labor_sql
go
SELECT convert(char(10),date,102)
FROM income
go

--- task 44 ---
use labor_sql
go
SELECT ship, battle,
       CASE result
	      WHEN 'sunk' THEN '����������'
              WHEN 'damaged' THEN '�����������'
	      WHEN 'ok' THEN '�����'
       END AS result
FROM outcomes
go

--- task 45 ---
use labor_sql
go
SELECT trip_no, [date], id_psg,
       '���: '+substring(place,1,1) as [row],
       '����: '+substring(place,2,2) as place
FROM pass_in_trip
go

--- task 46 ---
use labor_sql
go
SELECT trip_no, id_comp, plane, 
       concat(concat('from ',town_from), concat('to ',town_to)) as destination,
       time_out, time_in
FROM trip
go

--- task 47 ---
use labor_sql
go
SELECT LEFT(trip_no, 1) + RIGHT(trip_no, 1) 
+ LEFT(id_comp, 1) + RIGHT(id_comp, 1) 
+ LEFT(RTRIM(plane), 1) + RIGHT(RTRIM(plane), 1) 
+ LEFT(RTRIM(town_to), 1) + RIGHT(RTRIM(town_to), 1) 
+ LEFT(RTRIM(town_from), 1) + RIGHT(RTRIM(town_from), 1) 
+ LEFT(RTRIM(time_out), 1) + RIGHT(RTRIM(time_out), 1) 
+ LEFT(RTRIM(time_in), 1) + RIGHT(RTRIM(time_in), 1)
FROM trip
go

--- task 48 ---
use labor_sql
go
SELECT maker, COUNT(model) AS model_number
FROM product
WHERE type = 'pc'  
GROUP BY maker
HAVING COUNT(DISTINCT model) >=2
go

--- task 49 ---
use labor_sql
go
SELECT town_from,town_to,count(trip_no) races_number
FROM trip
GROUP BY grouping sets(town_from,town_to)
go

--- task 50 ---
use labor_sql
go
SELECT type AS printer_type, COUNT(model) AS models_number
FROM printer
GROUP BY type
go

--- task 51 ---
use labor_sql
go
SELECT model AS pc_models, COUNT(DISTINCT cd) AS cd_number,
       COUNT(DISTINCT model) AS models_number
FROM pc
GROUP BY grouping sets(cd, model)
go

--- task 52 ---
use labor_sql
go
SELECT trip_no, CAST((time_in - time_out) AS time)
FROM trip
go

--- task 53 ---
use labor_sql
go
SELECT point, convert(char(10),[date],102), 
       sum([out]) as sum_cash, min([out]) as min_cash, 
       max([out]) as max_cash 
FROM outcome
GROUP BY grouping sets((point, [date]),(point))
go

--- task 54 ---
use labor_sql
go
SELECT trip_no, substring(place, 1 , 1) AS number_of_row,
       COUNT(substring(place, 2 , 2)) AS number_of_place
FROM pass_in_trip
GROUP BY trip_no, substring(place, 1 , 1)
ORDER BY trip_no
go


--- task 55 ---
use labor_sql
go
SELECT 'A: ' + STR(COUNT(SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name))))
FROM passenger
WHERE SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name))  LIKE '[A]%'
UNION ALL
SELECT 'B: '+ STR(COUNT(SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)))) 
FROM [Passenger]
WHERE SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name))  LIKE '[B]%'
UNION ALL
SELECT 'S: '+ STR(COUNT(SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name)))) 
FROM [Passenger]
WHERE SUBSTRING(name, CHARINDEX(' ', name)+ 1, LEN(name))  LIKE '[S]%'
go






