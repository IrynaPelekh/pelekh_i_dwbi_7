use ip_library
go

insert into book
(ISBN, publisher_id, URL, price, title, edition, published, issue) values 
('978-6-175-85100-5',1, 'www.bukva.ua/', 470.00, 'Harry_Potter_and_the_Philosopher_Stone', 5, 500, 5),
('978-1-484-23444-0',11, 'www.apress.com/gp/book', 520.00, 'Beginning_Blockchain', 1, 100, 1),
('978-1-484-21964-5',11, 'www.apress.com/gp/book/', 510.00, 'Pro_SQL_Server_Internals', 2, 100, 2),
('978-0-349-14131-2',12, 'www.littlebrown.co.uk/books', 230.00, 'Girls_on_Fire', 1, 100, 1),
('978-1-786-49423-8',5, 'www.atlantic_books.co.uk/book/sloths/', 185.00, 'Sloths', 1, 100, 1)
go

select * from book
go



