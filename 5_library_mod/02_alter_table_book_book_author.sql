use ip_library
go

ALTER TABLE book
DROP COLUMN IF EXISTS title
go

ALTER TABLE book
DROP COLUMN IF EXISTS edition
go

ALTER TABLE book
DROP COLUMN IF EXISTS published
go

ALTER TABLE book
DROP COLUMN IF EXISTS issue
go


ALTER TABLE book
ADD title varchar(50) NOT NULL DEFAULT('Title'),
edition int NOT NULL DEFAULT(1) CHECK(edition>=1),
published date NULL,
issue int NOT NULL DEFAULT(1)
go

EXEC sp_rename @pk_name,'PK_book'

ALTER TABLE book
DROP CONSTRAINT PK_book

ALTER TABLE book
ADD CONSTRAINT PK_book_new
PRIMARY KEY (ISBN,issue)

ALTER TABLE author_book
DROP CONSTRAINT FK_author_book_ISBN
go