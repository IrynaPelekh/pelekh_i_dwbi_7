use ip_library
go

ALTER trigger tr_author_log ON author
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end

---- insert
if @operation = 'I'
begin
	insert into author_log
				(author_id_new,name_new,URL_new,operation_type,
				book_amount_new,issue_amount_new,total_edition_new)
	Select 
		inserted.author_id,
		inserted.name,
		inserted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition

	FROM author 
	inner join inserted on author.author_id = inserted.author_id
end

---- delete
if @operation = 'D'
begin
	insert into author_log
				(author_id_old,name_old,URL_old,operation_type,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		deleted.author_id,
		deleted.name,
		deleted.URL,
		@operation,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM deleted
end

--- update
if @operation = 'U'
	begin
		update author 
		set updated = getdate(), 
			updated_by = SYSTEM_USER
		from author 
			inner join inserted on author.author_id = inserted.author_id;

	insert into author_log
				(author_id_new,name_new,URL_new,author_id_old,name_old,URL_old,operation_type,
				book_amount_new,issue_amount_new,total_edition_new,
				book_amount_old,issue_amount_old,total_edition_old)
	Select 
		inserted.author_id,
		inserted.name,
		inserted.URL,
		deleted.author_id,
		deleted.Name,
		deleted.URL,
		@operation,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition
	FROM inserted INNER JOIN deleted ON inserted.author_id = deleted.author_id
		
	end
END;
GO

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO
