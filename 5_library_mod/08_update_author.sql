use ip_library
go

update author
set
    book_amount = 19, issue_amount = 50, total_edition = 20000
	where author_id = 4
go

update author
set book_amount = 12, issue_amount = 30, total_edition = 5000
where author_id = 3
go

update author
set book_amount = 17, issue_amount = 40, total_edition = 7000
where author_id = 2
go

update author
set book_amount = 25, issue_amount = 70, total_edition = 22000
where author_id = 1
go

update author
set book_amount = 5, issue_amount = 7, total_edition = 1000
where author_id = 5
go


select * from author
go

