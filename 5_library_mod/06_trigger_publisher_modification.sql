use ip_library
go

DROP TRIGGER IF EXISTS tr_publ_modif
go

CREATE trigger tr_publ_modif ON book
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
    UPDATE publisher
    SET publisher.book_amount=COALESCE((SELECT COUNT(*) FROM book
    WHERE book.publisher_id=publisher.publisher_id),0)
    WHERE publisher.publisher_id 
    IN (SELECT publisher_id FROM inserted UNION ALL SELECT publisher_id FROM deleted);

    UPDATE publisher
    SET publisher.issue_amount=COALESCE((SELECT COUNT(*) FROM book
    WHERE book.publisher_id=publisher.publisher_id),0)
    WHERE publisher.publisher_id 
    IN (SELECT publisher_id FROM inserted UNION ALL SELECT publisher_id FROM deleted);

    UPDATE publisher
    SET publisher.total_edition=COALESCE((SELECT COUNT(*) FROM book
    WHERE book.publisher_id=publisher.publisher_id),0)
    WHERE publisher.publisher_id 
    IN (SELECT publisher_id FROM inserted UNION ALL SELECT publisher_id FROM deleted);
END