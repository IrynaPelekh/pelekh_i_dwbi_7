use ip_library
go

insert into publisher
(publisher_id, name, URL,  updated, updated_by, book_amount, issue_amount, total_edition) values 
(NEXT VALUE FOR seq2_ip_library,'Apress', 'www.apress.com', NULL, NULL, 3000, 7000, 60000),
(NEXT VALUE FOR seq2_ip_library,'Little_Brown', 'www.littlebrown.co.uk', NULL, NULL, 4000, 8000, 70000),
(NEXT VALUE FOR seq2_ip_library, 'Athlantic_Books', 'www.atlantic_books.co.uk', NULL, NULL, 2500, 6000, 50000)
go

select * from publisher
go
