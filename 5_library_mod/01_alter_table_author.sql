use ip_library
go

ALTER TABLE author
DROP COLUMN IF EXISTS birthday
go

ALTER TABLE author
DROP COLUMN IF EXISTS book_amount
go

ALTER TABLE author
DROP COLUMN IF EXISTS issue_amount
go

ALTER TABLE author
DROP COLUMN IF EXISTS total_edition
go


ALTER TABLE author
ADD birthday date NULL,
    book_amount int NOT NULL DEFAULT(0) CHECK(book_amount>=0),
	issue_amount int NOT NULL DEFAULT(0) CHECK(issue_amount>=0),
	total_edition int NOT NULL DEFAULT(0) CHECK(total_edition>=0)
go