use ip_library
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS book_amount_old
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS issue_amount_old
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS total_edition_old
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS book_amount_new
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS issue_amount_new
go

ALTER TABLE author_log
DROP COLUMN IF EXISTS total_edition_new
go


ALTER TABLE author_log
ADD book_amount_old int NULL,
    issue_amount_old int NULL,
	total_edition_old int NULL,
	book_amount_new int NULL,
	issue_amount_new int NULL,
	total_edition_new int NULL
go