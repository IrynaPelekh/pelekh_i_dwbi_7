
--- task 1 ---
use labor_sql
go
SELECT pr.maker, pr.[type], pc.speed, pc.hd
FROM pc
INNER JOIN product
ON pc.model=pr.model
WHERE pc.hd <= 8
go

--- task 2 ---
use labor_sql
go
SELECT DISTINCT pr.maker
FROM pc
  INNER JOIN product
    ON pc.model=pr.model
WHERE pc.speed >= 600
go

--- task 3 ---
use labor_sql
go
SELECT DISTINCT pr.maker
FROM laptop
  INNER JOIN product
    ON l.model=pr.model
WHERE l.speed <= 500
go

--- task 4 ---
use labor_sql
go
SELECT DISTINCT l1.model AS l1_model, l2.model AS l2_model, l1.hd, l1.ram
FROM laptop
  INNER JOIN laptop
    ON l1.hd=l2.hd AND l1.ram=l2.ram 
       AND l1.model>l2.model
go

--- task 5 ---
use labor_sql
go
SELECT c1.country, c1.class AS bb_class, c2.class as bc_class
from classes
  INNER JOIN classes
    ON c1.country=c2.country
WHERE c1.[type]='bb' AND c2.[type]='bc'
go

--- task 6 ---
use labor_sql
go
SELECT DISTINCT pc.model, pr.maker
FROM pc
  INNER JOIN product AS pr
    ON pc.model = pr.model
WHERE pc.price < 600
go

--- task 7 ---
use labor_sql
go
SELECT DISTINCT p.model, pr.maker
FROM printer AS p
  INNER JOIN product AS pr
    ON p.model = pr.model
WHERE p.price > 300
go

--- task 8 ---
use labor_sql
go
SELECT pr.maker, pc.model, pc.price
FROM pc
  INNER JOIN product AS pr
    ON pc.model = pr.model
go

--- task 9 ---
use labor_sql
go
SELECT pr.maker, pr.model, pc.price
FROM product AS pr
  LEFT JOIN pc
    ON pc.model = pr.model
WHERE pr.[type] = 'pc'
go

--- task 10 ---
use labor_sql
go
SELECT pr.maker, pr.[type], l.model, l.speed
FROM laptop AS l
  INNER JOIN product AS pr
    ON l.model = pr.model
WHERE l.speed > 600
go

--- task 11 ---
use labor_sql
go
SELECT s.[name], c.displacement
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class = c.class
go

--- task 12 ---
use labor_sql
go
SELECT o.ship, b.[name], b.[date]
FROM outcomes AS o
  INNER JOIN battles AS b
    ON o.battle = b.[name]
WHERE NOT EXISTS(SELECT * FROM outcomes AS o2 
    WHERE (o2.result='sunk' OR o2.result='damaged') AND o2.ship=o.ship)
go

--- task 13 ---
use labor_sql
go
SELECT s.[name], c.country
FROM ships AS s
  INNER JOIN classes AS c
    ON s.class = c.class
go

--- task 14 ---
use labor_sql
go
SELECT DISTINCT t.plane, c.[name] AS company_name
FROM trip AS t
  INNER JOIN company AS c
    ON t.id_comp = c.id_comp
WHERE t.plane = 'Boeing'
go

--- task 15 ---
use labor_sql
go
SELECT DISTINCT p.id_psg, p.[name], pit.[date]
FROM passenger AS p
  INNER JOIN pass_in_trip AS pit
    ON p.id_psg = pit.id_psg
go

--- task 16 ---
use labor_sql
go
SELECT pc.model, pc.speed, pc.hd
FROM pc
  INNER JOIN product AS pr
    ON pc.model = pr.model
WHERE (pc.hd = 10 OR pc.hd = 20) AND pr.maker = 'A'
ORDER BY pc.speed
go

--- task 17 ---
use labor_sql
go
SELECT *
FROM product
 PIVOT (COUNT(model) 
        FOR [type] in (pc, laptop, printer)) AS p
go

--- task 18 ---
use labor_sql
go
SELECT *
FROM (SELECT screen, price, 'avarage price' AS _avg
      FROM laptop) AS l
 PIVOT (AVG(price)
        FOR screen IN ([11],[12],[14],[15])) AS P
go

--- task 19 ---
use labor_sql
go
SELECT * 
FROM laptop AS l
  CROSS APPLY (SELECT maker
               FROM product AS pr
			   WHERE pr.model = l.model) AS m
go

--- task 20 ---
use labor_sql
go
SELECT * 
FROM laptop AS l1
  CROSS APPLY (SELECT MAX(l2.price) AS max_price
   FROM laptop AS l2  INNER JOIN product AS pr1
     ON l2.model=pr1.model
       WHERE pr1.maker=(SELECT pr2.maker FROM product AS pr2	
	WHERE pr2.model=l1.model)) AS m
go

--- task 21 ---
use labor_sql
go
WITH c
AS
(SELECT code, model, speed, ram, hd, price, screen, 
        ROW_NUMBER() OVER(ORDER BY model, code) AS row_num
FROM laptop)
SELECT c1.code, c1.model, c1.speed, c1.ram, c1.hd, c1.price, c1.screen,
       l.code, l.model, l.speed, l.ram, l.hd, l.price, l.screen
FROM c AS c1
  CROSS APPLY (SELECT TOP 1 * FROM c AS c2
	WHERE c2.row_num>c1.row_num
        ORDER BY c2.row_num) AS l 
ORDER BY c1.model,c1.code
go

--- task 22 ---
use labor_sql
go
WITH c
AS
(SELECT code, model, speed, ram, hd, price, screen, 
        ROW_NUMBER() OVER(ORDER BY model, code) AS row_num
FROM laptop)
SELECT c1.code, c1.model, c1.speed, c1.ram, c1.hd, c1.price, c1.screen,
       l.code, l.model, l.speed, l.ram, l.hd, l.price, l.screen
FROM c AS c1
  OUTER APPLY (SELECT TOP 1 *
               FROM c AS c2
			   WHERE c2.row_num>c1.row_num
			   ORDER BY c2.row_num) AS l 
ORDER BY c1.model,c1.code
go

--- task 23 ---
use labor_sql
go
SELECT t2.maker, t2.model, t1.[type]
FROM (SELECT DISTINCT p1.[type]
  FROM product as p1) AS t1
  CROSS APPLY (SELECT TOP 3 p2.maker, p2.model
        FROM product as p2
	    WHERE p2.[type]=t1.[type]
	     ORDER BY p2.model) AS t2
go

--- task 24 ---
use labor_sql
go
SELECT code, [name], [value]
FROM (SELECT code, speed, ram, CAST(hd AS smallint) AS hd, CAST(screen AS smallint) as screen
      FROM laptop) AS l
  UNPIVOT ([value] FOR [name] IN (speed, ram, hd, screen)) AS u
go










