use ip_library
go

insert into author
(author_id, name, URL,  updated, updated_by) values 
(NEXT VALUE FOR seq1_ip_library,'Robert_Christie', 'www.r_christy.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Sophie_Duffu', 'www.s_duffu.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Dale_Pendell', 'www.d_pendel.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'J_K_Rowling', 'www.j_k_rowling.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Petter_Mail', 'www.p_mail.com', NULL, NULL)
go
