use ip_library
go

DROP SYNONYM IF EXISTS author_synonym
go


CREATE SYNONYM dbo.author_synonym FOR ip_library.dbo.author
go