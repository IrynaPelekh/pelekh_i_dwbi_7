use ip_library
go

DROP VIEW IF EXISTS publisher_view
go

CREATE VIEW publisher_view
AS
SELECT * FROM publisher
go

SELECT * FROM publisher_view
go