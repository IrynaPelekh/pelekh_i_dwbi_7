use ip_library;
go

DROP SYNONYM IF EXISTS ip_library_synonym
go

CREATE SYNONYM ip_library_synonym FOR ip_library
go
