use ip_library
go

insert into book
(ISBN, publisher_id, URL, price) values 
('978-5-389-14878-9',1, 'www.bukva.ua', 100.00),
('978-1-785-30154-4',2, 'www.blackandwhitepublishing.com', 120.00),
('978-1-908-24832-9',3, 'www.legendtimesgroup.co.uk/legend-press/books', 110.00),
('978-1-626-72234-7',4, 'www.us.macmillan.com', 220.00),
('978-1-562-79138-4',5, 'www.mercuryhouse.org', 185.00)
go



