use ip_library;
go

drop view if exist author_view
go

CREATE VIEW author_view
AS
SELECT * FROM author
go

SELECT * FROM author_view
go