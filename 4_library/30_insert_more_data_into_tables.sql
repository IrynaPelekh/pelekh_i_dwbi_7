use ip_library
go

insert into author
(author_id, name, URL,  updated, updated_by) values 
(NEXT VALUE FOR seq1_ip_library,'Authorname6', 'www.Authorname6.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Authorname7', 'www.Authorname7.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Authorname8', 'www.Authorname8.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Authorname9', 'www.Authorname9.com', NULL, NULL),
(NEXT VALUE FOR seq1_ip_library,'Authorname10', 'Authorname10', NULL, NULL)
go
----------------------------

insert into publisher
(publisher_id, name, URL,  updated, updated_by) values 
(NEXT VALUE FOR seq2_ip_library,'Publishername6', 'www.Publishername6.ua', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Publishername7', 'www.Publishername7.com', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Publishername8', 'www.Publishername8.co.uk', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Publishername9', 'www.Publishername9.com', NULL, NULL),
(NEXT VALUE FOR seq2_ip_library,'Publishername10', 'www.Publishername10.org', NULL, NULL)
go
---------------------------

insert into book
(ISBN, publisher_id, URL, price) values 
('978-6-389-14878-9',6, 'www.Publishername6.ua', 110.00),
('978-2-785-30154-4',7, 'www.Publishername7.com', 130.00),
('978-3-908-24832-9',8, 'www.Publishername8/books', 120.00),
('978-4-626-72234-7',9, 'www.Publishername9.com', 210.00),
('978-5-562-79138-4',10, 'www.Publishername10.org', 185.00)
go
---------------------------

insert into author_book
(author_book_id, ISBN, author_id, updated, updated_by) values 
(6,'978-6-389-14878-9', 10, NULL, NULL),
(7, '978-2-785-30154-4',9, NULL, NULL),
(8, '978-3-908-24832-9',7, NULL, NULL),
(9, '978-4-626-72234-7',6, NULL, NULL),
(10, '978-5-562-79138-4',8, NULL, NULL)
go











