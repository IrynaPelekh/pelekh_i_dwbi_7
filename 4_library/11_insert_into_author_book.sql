use ip_library
go

insert into author_book
(author_book_id, ISBN, author_id, updated, updated_by) values 
(1,'978-5-389-14878-9', 5, NULL, NULL),
(2, '978-1-785-30154-4',4, NULL, NULL),
(3, '978-1-908-24832-9',2, NULL, NULL),
(4, '978-1-626-72234-7',1, NULL, NULL),
(5, '978-1-562-79138-4',3, NULL, NULL)
go
