use ip_library
go

DROP SYNONYM IF EXISTS book_synonym
go


CREATE SYNONYM dbo.book_synonym FOR ip_library.dbo.book
go