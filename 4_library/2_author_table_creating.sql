use ip_library
go

drop table if exists author
go

create table author(
author_id int NOT NULL PRIMARY KEY,
name varchar(50) not null,
URL varchar(50) not null default('www.author_name.com'),
inserted date not null default (getdate()),
inserted_by varchar(50) not null default(system_user),
updated	date null,
updated_by varchar(50) null
)
