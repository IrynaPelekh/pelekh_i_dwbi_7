use ip_library
go

drop table if exists publisher
go

create table publisher(
publisher_id int NOT NULL PRIMARY KEY,
name varchar(50) not null,
URL varchar(50) default('www.publisher_name.com') not null,
inserted date not null default getdate(),
inserted_by varchar(50) not null default system_user,
updated	date,
updated_by varchar(50)
)