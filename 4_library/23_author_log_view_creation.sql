use ip_library
go

DROP VIEW IF EXISTS author_log_view
go

CREATE VIEW author_log_view
AS
SELECT * FROM author_log
go

SELECT * FROM author_log_view
go