use ip_library;
go

DROP VIEW IF EXISTS book_view
go

CREATE VIEW book_view
AS
SELECT * FROM book
go

SELECT * FROM book_view
go