use ip_library
go

ALTER TABLE book 
  ADD CONSTRAINT FK_book_publisher
  FOREIGN KEY(publisher_id) REFERENCES publisher(publisher_id)
go

ALTER TABLE author_book
 ADD CONSTRAINT FK_author_book_ISBN
 FOREIGN KEY(ISBN) REFERENCES book(ISBN)
 ON UPDATE CASCADE
 ON DELETE NO ACTION
go

 ALTER TABLE author_book
  ADD CONSTRAINT FK_author_book_id
  FOREIGN KEY(author_id) REFERENCES author(author_id)
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
go


ALTER TABLE author_book
  ADD CONSTRAINT UK_ISBN_author
  UNIQUE(ISBN,author_id)
go