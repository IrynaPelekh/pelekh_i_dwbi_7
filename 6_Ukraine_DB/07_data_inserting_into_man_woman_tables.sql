use people_ua_db
go

insert into man_name([name])
select [name] from name_list_identity
where sex='m'
go

insert into woman_name([name])
select [name] from name_list_identity
where sex='w'
go

declare @surname nvarchar(20),
        @sex nchar(1),
        @amount int,
	@man_names int,
	@woman_names int,
	@gen int,
	@name_gen nvarchar(20),
	@birthday_gen Date,
	@region_gen_id int,
	@materniny_ward_gen_id int,
	@sex_gen nchar(1),
	@names int,
	@i int

set @man_names=(select max(id) from man_name)
set @woman_names=(select max(id) from woman_name)
set @names=(select max(id) from name_list_identity)
set @i=1;

declare c cursor
for select surname, amount, sex from surname_list_identity

open c
fetch next from c into @surname, @amount, @sex

while @@FETCH_STATUS=0
  begin
    while @i>0
	  begin
	    set @birthday_gen=(select dateadd(day,ABS((CHECKSUM(NEWID()))) % 27758,'19250101'))
	    set @region_gen_id=(ABS((CHECKSUM(NEWID()))) % 25+1)
		set @materniny_ward_gen_id=(ABS((CHECKSUM(NEWID())))%5+1)
	    if @sex='m'
		  begin
			set @gen=(ABS((CHECKSUM(NEWID()))) % @man_names+1)
			set @name_gen=(SELECT TOP 1 name FROM man_name WHERE id >= @gen)
	        INSERT INTO person(surname,[name],sex,birthday,person_region_id,materniny_ward_id)
	        VALUES(@surname,@name_gen,'m',@birthday_gen,@region_gen_id,@materniny_ward_gen_id)
		  end
		  
		  if @sex='w'
		  begin
			set @gen=(ABS((CHECKSUM(NEWID()))) % @woman_names+1)
			set @name_gen=(SELECT TOP 1 name FROM woman_name WHERE id >= @gen)
	        INSERT INTO person(surname,[name],sex,birthday,person_region_id,materniny_ward_id)
	        VALUES(@surname,@name_gen,'w',@birthday_gen,@region_gen_id,@materniny_ward_gen_id)
		  end

		  if @sex is null
		    begin
			  set @gen=(ABS((CHECKSUM(NEWID()))) % @names+1)
			  set @name_gen=(SELECT [name] FROM name_list_identity WHERE id = @gen)
			  set @sex_gen=(SELECT sex FROM name_list_identity WHERE id = @gen)
	          INSERT INTO person(surname,[name],sex,birthday,person_region_id,materniny_ward_id)
	          VALUES(@surname,@name_gen,@sex_gen,@birthday_gen,@region_gen_id,@materniny_ward_gen_id)
			end
	  
	  SET @amount=@amount-1
	  print 'Now row'+cast(@i as varchar(10))
	  set @i=@i+1
	  end
  fetch next from c into @surname, @amount, @sex
  end

close c
deallocate c
go