use people_ua_db
go

---------inserting checking
declare @i int
set @i=1

while @i<100
  begin
    insert into region([name],main_city,city_quantity,village_quantity)
    values('region'+rtrim(cast(@counter as varchar(5))), 'city'+rtrim(cast(@counter as varchar(5))),'city_quantity',
	       'village_quantity')
    set @i=@i+1
  end
go

declare @region_gen int
set @region_gen=(select max(id) from region)

set @i=1
while @i<1000000
  begin
    insert into person(surname,[name],sex,birthday,person_region_id)
    values('surname'+rtrim(cast(@counter as varchar(7))),'name'+rtrim(cast(@counter as varchar(7))),
	       case when @counter%2=0 then 'w' else 'm' end, dateadd(day,ABS((CHECKSUM(NEWID()))) % 27758,'19250101'),
		   (ABS((CHECKSUM(NEWID()))) % @n_region+1))
    set @i=@i+1
  end
go


