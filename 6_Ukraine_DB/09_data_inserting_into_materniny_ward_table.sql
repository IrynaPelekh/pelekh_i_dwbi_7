use people_ua_db
go

declare @i int
set @i=1;


while @i<=200
 begin
	insert into materniny_ward([name],mw_region_id)
	values('materniny_ward'+rtrim(cast(@i as varchar(3))))
	set @i=@i+1
 end

declare @person_gen int
declare @materniny_ward_gen int
set @i=1
set @person_gen=(select max(id) from person)
set @materniny_ward_gen=(select max(id) from materniny_ward)

 while @i<=10000
   begin
     insert into person_materniny_ward(person_id,materniny_ward_id)
     values(ABS((CHECKSUM(NEWID()))) % @person_gen+1,ABS((CHECKSUM(NEWID()))) % @materniny_ward_gen+1)
     set @i=@i+1
   end

go