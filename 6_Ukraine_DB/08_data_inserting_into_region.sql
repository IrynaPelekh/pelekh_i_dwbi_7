use people_ua_db
go


INSERT INTO region (name, main_city)
VALUES ('Odeska', 'Odessa'),
       ('Dnipropetrovska', 'Dnipro'),
       ('Chernigivska', 'Chernigiv'),
       ('Kharkivska', 'Kharkiv'),
       ('Zhytomyrska', 'Zhytomyr'),
       ('Poltavska', 'Poltava'),
       ('Khersonska', 'Kherson'),
       ('Kyivska', 'Kyiv'),
       ('Zaporizka', 'Zaporizhzhia'),
       ('Luganska', 'Lugansk'),
       ('Donezka', 'Donezk'),
       ('Vinnyzka', 'Vinnyzya'),
       ('Avton_Respubl_Krym', 'Simferopol'),
       ('Mykolaivska', 'Mykolaiv'),
       ('Kirovogradska', 'Kropyvnyzky'),
       ('Sumska', 'Sumy'),
       ('Lvivska', 'Lviv'),
       ('Cherkaska', 'Cherkasy'),
       ('Khmelnyzka', 'Khmelnyzk'),
       ('Volynska', 'Luzk'),
       ('Rivnenska', 'Rivne'),
       ('Ivano-Frankivska', 'Ivano-frankivsk'),
       ('Ternopilska', 'Ternopil'),
       ('Zakarpatska', 'Zakarpattya'),
       ('Chernivezka', 'Chernivtsi')
        
go

