use people_ua_db
go

create clustered index ind_c_person_id on person(id)
with (fillfactor=30, pad_index=on)
go

create clustered index ind_c_region_id on region(id)
with (fillfactor=40, pad_index=on)
go

create unique index ind_u_region_name on region([name])
with (fillfactor=20, pad_index=on)
go


create nonclustered index ind_nc_person_name on person([name])
with (fillfactor=50, pad_index=on)
go

create nonclustered index ind_nc_person_birthday_sex on person(birthday)
include(sex)
go