use test_trans
go

set transaction isolation level read uncommitted
go

begin transaction
select * from product

--we have read uncomitted changes

--switch to first connection
rollback transation

--still in connection 1
use test_trans
go

select * from product
where id=1

begin transaction
 update product
   set price+=10
   where id=1

 select * from product
 where id=1

--switch to second connection
--tran is not committed yet

set transaction isolation level read committed
use test_trans
go

select * from product
where id=1

--ts is blocked, so we cant read uncommited changes

--back to 1-st connection
commit transaction

--in second connection ts is completed

