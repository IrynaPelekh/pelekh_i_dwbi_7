use test_trans
go

drop table if exists product
go

create table product (
id int not null identity(1,1) primary key,
[name] varchar(20) not null,
price numeric(5,2) not null
)
go

insert into product ([name], price)
values ('product_1',10),
	   ('product_2',20),
	   ('product_3',30)
go