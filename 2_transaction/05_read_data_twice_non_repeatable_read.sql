
use test_trans
go

begin transaction

select * from product
where id=2

---- other statements
---- testing 2 connections
--- switch to second connection
use test_trans
go

---- wait, another transaction updates data
waitfor delay '00:00:10'

begin transaction
update product
set price+=25
where id=2
commit transaction

---- read data again

select * from product
where id=2

commit transaction

--we have 2 different result in one ts, not repeatable read

------------------- solution

SET TRANSACTION ISOLATION LEVEL REPEATABLE READ
use test_trans
go

begin transaction
select * from product
where id=2

--ts is not comitted

--switch to second connection
use test_trans
go

begin transaction
update product
set price+=25
where id=2
commit transaction

--ts is blocked

--switch to first connection
select * from product
where id=2

commit transaction
--we have the same result as in first read
--now updates are applied





