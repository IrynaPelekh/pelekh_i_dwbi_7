use test_trans
go

begin transaction
select * from product
where name in('product_1','product_2')

--- ts is not comitted

--- switch to second connection

use test_trans
go

insert into product([name],price)
values('product_1',50)

--- switch to first connection

select * from product
where name in('product_1','product_2')
commit transaction

--- we see this fantom row within 1 ts

---------- solution

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

use test_trans
go

begin transaction
select * from product
where name in('product_1','product_2')

--- ts is not comitted

--- switch to second connection

use test_trans
go

insert into product([name],price)
values('product_1',600)

--- ts is blocked

--- switch to first connection
select * from product
where name in('product_1','product_2')

--we dont see fantom row
commit transaction
--- now insertion is completed
