-- first transaction updada data
use test_trans
go

begin transaction

update product 
set price+=5
   where id=1

--wait, first transaction read uncommitted data

waitfor delay '00:00:05'

rollback transaction

